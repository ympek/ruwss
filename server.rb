require 'socket'
require "./http_request.rb"

def ret_http_switching_protocols_response(sock, secretcode)
  resp = ""
  resp += "HTTP/1.1 101 Switching Protocols\n"
  resp += "Upgrade: websocket\n"
  resp += "Connection: Upgrade\n"
  resp += "Sec-WebSocket-Accept: " + secretcode + "\n\n"
  sock.puts(resp)
end

def is_single_newline(line)
  return line == "\r\n"
end


def unmask_message(mask, payload)
  mask_bytes = mask.bytes
  payload_bytes = payload.bytes
  ret = []
  payload_bytes.each_with_index do |byte, i|
    ret[i] = byte ^ mask_bytes[i % 4]
  end
  ret.pack("C*")
end

port = 2000

TCPServer.open("127.0.0.1", port) do |serv|
  puts "Listening on port number #{port}"
  client = serv.accept # Wait for a client to connect
  req = ""
  while (line = client.gets) do
    puts line
    puts line.length
    req += line
    if (is_single_newline(line))
      puts req
      wrapped_req = HttpRequest.new(req)
      puts wrapped_req.sec_websocket_key
      secretcode = wrapped_req.secret_code_for_resp
      puts secretcode
      ret_http_switching_protocols_response(client, secretcode)
      break # wyjdzmy se z petli i zobaczmy co dalej;
    end
  end

  # wait for messages infinitely.
  while(true) do
    puts "Po petli"
    anymessages = client.recv(128)
    bits = anymessages.unpack("B*B*B*B*")
    bits_as_int = anymessages.unpack("N")
    # well now we need to unmask.
    # maska jest to sa bajty nr
    payload_length = anymessages.byteslice(1).unpack("C*").first & 0x7F
    masking_key = anymessages.byteslice(2, 5) # pod warunkiem, ze payload jest krotki.
    masked_payload = anymessages.byteslice(6, payload_length)

    msg = unmask_message(masking_key, masked_payload)
    puts "Nasze dane:"
    puts payload_length
    puts msg

    # mask_to_puts = anymessages & 0xFFFF;
    #
    #puts bits_as_int[0].to_s(16)
    #puts "____"
    #puts masking_key
    #puts "____"
    #puts bits
    #puts "____"
    ## puts mask_to_puts
    #puts "____"
    #puts masking_key.unpack("B*B*B*B*")

    sleep 1 # wonder if it is needed


    # jak rozpakowac payload...
##
#   Octet i of the transformed data ("transformed-octet-i") is the XOR of
#   octet i of the original data ("original-octet-i") with octet at index
#   i modulo 4 of the masking key ("masking-key-octet-j"):
#
#     j                   = i MOD 4
#     transformed-octet-i = original-octet-i XOR masking-key-octet-j
#
#   The payload length, indicated in the framing as frame-payload-length,
#   does NOT include the length of the masking key.  It is the length of
#   the "Payload data", e.g., the number of bytes following the masking
#   key.

  end
  # if line.chomp == "GET / HTTP/1.1"
  #   ret_http_switching_protocols_response(client)
  # end
end

