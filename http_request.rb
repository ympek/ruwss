require 'digest'
require "base64"

$WS_GUID = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"

class HttpRequest
  def initialize(req)
    splitted_req = req.split("\n")
    # remove method from beginning
    @method = splitted_req.shift

    @req_hash = Hash.new
    splitted_req.map do |item|
      pair = item.split(":")
      key = pair[0].strip.to_sym
      if pair[1]
        @req_hash[key] = pair[1].strip
      else
        puts "Pair[1] is nil. and pair[0] is " + pair[0]
      end
    end
  end

  def sec_websocket_key
    @req_hash[:"Sec-WebSocket-Key"]
  end

  def secret_code_for_resp
    concatenated = sec_websocket_key + $WS_GUID
    sha1ed_code = Digest::SHA1.digest(concatenated)
    base64encoded = Base64.encode64(sha1ed_code)
    base64encoded.strip # there was \n at the end
  end
end
