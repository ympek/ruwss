require "test/unit"

require "./http_request.rb"

class TestHttpRequest < Test::Unit::TestCase

  def test_given_example_data_from_rfc_should_return_example_result_from_rfc
    # I wonder if using squiggly heredoc will break things.
    @stub_request = <<~REQ
    GET / HTTP/1.1
    Host: localhost:2000
    User-Agent: unnecessary
    Sec-WebSocket-Extensions: permessage-deflate
    Sec-WebSocket-Key: dGhlIHNhbXBsZSBub25jZQ==
    Pragma: no-cache
    Upgrade: websocket
    REQ

    sut = HttpRequest.new(@stub_request)

    assert_equal "s3pPLMBiTxaQ9kYGzzhZRbK+xOo=", sut.secret_code_for_resp
  end
end
